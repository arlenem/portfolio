if ($(".module-event-features .event-wrapper").length) {
	if ( parseInt($(".module-event-features").css("width")) >= 900 ) {
		$(".module-event-features .event-wrapper").each(function() {
			$(this).eventsImageSize();
		});
	}
};

$.fn.eventsImageSize = function() {
	var holder = $(this);
	var h = holder.find(".text-wrapper")[0].getBoundingClientRect().height;
	holder.find(".image-wrapper").css("height", h);
	$(window).load(function(){
		var h = holder.find(".text-wrapper")[0].getBoundingClientRect().height;
		holder.find(".image-wrapper").css("height", h);
	});
};