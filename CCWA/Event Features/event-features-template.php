<?php 
/**
 * The template for displaying Modules in the event features format
 *
 * @package WordPress
 * @subpackage Ibel_Agency_Theme_2.0
 * @since 2.0
 ****************************************************************
 ****************************************************************/

// Initalize the global module variable
global $module;
$counter = 0;
$now = strtotime('now');

// Create the HTML Wrapper ?>
<section class="module-event-features">
	<article>
		<div class="match-height row clear-after">
			<div class="column-left col-md-6">
				<?php
				if ($module['module_fields']['left_link_url']) { 
					echo '<p class="feature-link-wrapper"><a class="a2 white-text" href="'.$module['module_fields']['left_link_url'].'">'. $module['module_fields']['left_link_text']. '<span></span></a></p>'; 
				}

				if (is_array($module['module_fields']['event_list']) && !empty($module['module_fields']['event_list'])) {
					foreach ($module['module_fields']['event_list'] as $e) {
						if ($counter < 3) {
							$date = tribe_get_start_date( $event = $e['event']->ID, $display_time = TRUE, $date_format = 'F j, Y, g:ia' );
							if ($now < strtotime($date)) {
								$event = get_fields($e['event']->ID);
								$permalink = get_permalink($e['event']->ID);
								if (tribe_event_is_all_day($e->ID)){
									$e_date = tribe_get_start_date ($e['event']->ID, $display_time = false, $date_format = 'F j, Y');
								} else {
									$start = tribe_get_start_date ($e['event']->ID, $display_time = false, $date_format = 'F j, Y');
									$end = tribe_get_end_date ($e['event']->ID, $display_time = false, $date_format = 'F j, Y');
									if ($start == $end) {
										$start = tribe_get_start_date ($e['event']->ID, $display_time = true, $date_format = 'F j, Y, g:i a');
										$end = tribe_get_end_date ($e['event']->ID, $display_time = true, $date_format = ' - g:i a');
										$e_date = $start . $end;
									} else {
										$start = tribe_get_start_date ($e['event']->ID, $display_time = true, $date_format = 'F j, Y, g:i a');
										$end = tribe_get_end_date ($e['event']->ID, $display_time = true, $date_format = ' - F j, Y, g:i a');
										$e_date = $start . $end;
									}
								}

								echo '<div class="event-wrapper white-background box-shadow">';
									echo '<div class="row">';
										echo '<div class="col-md-4 col-sm-4 image-wrapper">';
											echo '<div class="image-container">';
												if ($event['event_image']) {
													$img_srcset = wp_get_attachment_image_srcset($event['event_image']['id']);
													echo '<div><img src="'.$event['event_image']['sizes']['medium'].'" srcset="'.$img_srcset.'" sizes="(max-width: 900px) 50vw, (max-width: 767px) 100vw, 25vw" alt="'.$event['event_image']['title'].'"></div>';
												}
											echo '</div>';
										echo '</div>';
										echo '<div class="col-md-8 col-sm-8 text-container">';
											echo '<div class="text-wrapper">';
												//echo '<h6 class="dark-grey-text">'.$date.'</h6>';
												echo '<h6 class="dark-grey-text">'.$e_date.'</h6>';
												echo '<h4>'. $e['event']->post_title .'</h4>';
												if ($event['location']) {
													echo '<p class="text-location">'.$event['location'].'</p>';
												}
												echo '<p class="link-wrapper button-wrapper">';
													echo '<a class="a3 button" href="'.$event['sign_up_link'].'">Sign Up</a>';
													echo '<a class="a2" href="'.$permalink.'">Details<span></span></a>';
												echo '</p>';
											echo '</div>';
										echo '</div>';
									echo '</div>';
								echo '</div>';
								$counter++;
							}
						}
					} 
				}?>
				<?php if ($counter < 3) {
					if (is_array($module['module_fields']['half_module_left']) && !empty($module['module_fields']['half_module_left'])) {
						foreach ($module['module_fields']['half_module_left'] as $half) {
							$term = wp_get_post_terms( $half->ID, 'half_module_type');
							$module['half']  = get_fields($half->ID);
							echo "<div class='half-module module-".$term[0]->slug."' id='".$half->post_name."'>";
								get_template_part('half-module-templates/'.$term[0]->slug.'-template');
							echo "</div>";
						}
					}
				} ?>
			</div>
			<div class="column-right col-md-6">
				<?php
				if ($module['module_fields']['right_link_url']) { 
					echo '<p class="feature-link-wrapper"><a class="a2 white-text" href="'.$module['module_fields']['right_link_url'].'">'. $module['module_fields']['right_link_text']. '<span></span></a></p>'; 
				}

				if (is_array($module['module_fields']['half_module_right']) && !empty($module['module_fields']['half_module_right'])) {
					foreach ($module['module_fields']['half_module_right'] as $half) {
						$term = wp_get_post_terms( $half->ID, 'half_module_type');
						$module['half']  = get_fields($half->ID);
						echo "<div class='half-module module-".$term[0]->slug."' id='".$half->post_name."'>";
							get_template_part('half-module-templates/'.$term[0]->slug.'-template');
						echo "</div>";
					}
				} ?>
			</div>

		</div>
		<div class="background-layer <?php if ($module['module_fields']['background_color']) { echo $module['module_fields']['background_color']; } else { echo "white-background"; } ?>">
		</div>
	</article>
</section>