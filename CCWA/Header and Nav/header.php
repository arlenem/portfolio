<?php
/**
 * The Header for the Ibel Agency Theme
 *
 * Displays all of the header and navigation
 *
 * @package WordPress
 * @subpackage Ibel_Agency_Theme_2.0
 * @since 2.0
 */

$themeURL =  get_stylesheet_directory_uri();
$url = esc_url( home_url( '/' ) );

global $module;

?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<!-- force IE out of compatability mode for any version -->
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<!-- force mobile view 100% -->
	<meta name="viewport" content="width=device-width">
	<link rel="icon" href="<?php echo $themeURL;  ?>/img/favicon.ico">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
</head>

<body <?php body_class(); ?>>
	<div class='site-container pos-relative'>
		<header class="site-header" role="banner">
				<article class='pos-relative clear-after fullsize'>
					<div class='logo-wrapper'>
						<a class='logo-link' href="<?php echo $url; ?>"></a>
					</div>

					<div class='menu-block'>
						<div class='upper-menu black-background clear-after pos-relative'>
							<!--google translation -->
							<div id="google_translate_element"><p class="white-text translate">Translate<span><img src="<?php echo $themeURL;  ?>/img/Translate-Text.png"></span></p></div>
								<div id="google_translate_element"></div><script type="text/javascript">
									function googleTranslateElementInit() {
									  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.HORIZONTAL, autoDisplay: false}, 'google_translate_element');
									}
								</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
							<div class='contact'>
								<p>
									<a class="subnav white-text" href="<?php echo $url; ?>news">News</a>
									<a class="subnav white-text" href="<?php echo $url; ?>contact">Contact</a>
									<a href="https://www.facebook.com/columbusworldaffairs/" target="_blank"><img class="normal-image" src="<?php echo $themeURL;  ?>/img/Facebook-white.png"><img class="hover-image" src="<?php echo $themeURL;  ?>/img/Facebook-BrandBlue.png"></a>
									<a href="https://twitter.com/cbusccwa" target="_blank"><img class="normal-image" src="<?php echo $themeURL;  ?>/img/Twitter-white.png"><img class="hover-image" src="<?php echo $themeURL;  ?>/img/Twitter_BrandBlue.png"></a>
									<a href="https://www.linkedin.com/company/columbus-council-on-world-affairs" target="_blank"><img class="normal-image" src="<?php echo $themeURL;  ?>/img/LinkedIn-white.png"><img class="hover-image" src="<?php echo $themeURL;  ?>/img/LinkedIn-BrandBlue.png"></a>
								</p>
								
							</div>
							<div class="menu-tab">
								<div><span></span></div>
							</div>
							<div class="search-outer">
								<div class="search-wrapper"><span><img src="<?php echo $themeURL;  ?>/img/Search-Nav-white.png"></span>
									<form method="get" id="searchform" action="<?php echo $url ?>">
											<input type="text" class="field" name="s" id="s" placeholder="Search..." /><span><img src="<?php echo get_stylesheet_directory_uri();?>/img/Search-black.png"></span>
									</form>
								</div>
							</div>
						</div>

						<div class="menu-wrapper ease-transition clear-after">
							<?php wp_nav_menu( array( 'menu' => 'main')); ?>
						</div>

					</div>
				</article>	

		</header><!-- end of header -->
		<div class="header-buffer"></div>
		<div class="mobile-menu-wrapper black-background">
			<div class="mobile-menu-inner">
				<?php wp_nav_menu( array( 'menu' => 'mobile')); ?>
				<div class="mobile-search-wrapper clear-after"><span><img src="<?php echo $themeURL;  ?>/img/Search-Nav-white.png"></span>
					<form method="get" id="searchform" action="<?php echo $url; ?>">
							<input type="text" class="field" name="s" id="s" placeholder="" /><span><img src="<?php echo get_stylesheet_directory_uri();?>/img/Search-black.png"></span>
					</form>
				</div>
				<div class="upper-menu">
					<a href="https://www.facebook.com/columbusworldaffairs/"><img class="normal-image" src="<?php echo $themeURL;  ?>/img/Facebook-white.png"><img class="hover-image" src="<?php echo $themeURL;  ?>/img/Facebook-BrandBlue.png"></a>
					<a href="https://twitter.com/colworldaffairs?lang=en"><img class="normal-image" src="<?php echo $themeURL;  ?>/img/Twitter-white.png"><img class="hover-image" src="<?php echo $themeURL;  ?>/img/Twitter_BrandBlue.png"></a>
					<a href="https://www.linkedin.com/company/columbus-council-on-world-affairs"><img class="normal-image" src="<?php echo $themeURL;  ?>/img/LinkedIn-white.png"><img class="hover-image" src="<?php echo $themeURL;  ?>/img/LinkedIn-BrandBlue.png"></a>
				</div>
			</div>
		</div>
		</div>
		<div class="side-box-wrapper">
			<div class="side-box-inner clear-after">
				<p class="donate-box orange-background"><a class="a3" href="https://columbuscouncil.networkforgood.com/projects/19667-council-membership" target="_blank">DONATE</a></p>
				<p class="watch-box bright-blue-background"><a class="a3" href="https://vimeo.com/channels/101828" target="_blank">WATCH</a></p>
				<p class="livestream-box black-background"><a href="https://livestream.com/columbusworldaffairs" target="_blank"><img src="<?php echo $themeURL; ?>/img/Livestream.png"></a></p>
			</div>
		</div>



