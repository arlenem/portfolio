(function($){
	//Main Navigation display functions
	//Search form expand and hide
	if($(".search-wrapper").length && $(".search-outer").css("display") == 'block') {
	    $(".search-wrapper").searchExpand();
	}
	if($("header #menu-main").length) {
	    $("header #menu-main").menuHover();
	}
	if ($("header .menu-main-container").css("display") === "none") {
		//mobile nav
		$( "header .menu-block").mobileMenu();
	};


	$.fn.searchExpand = function() {
		var holder = $(this);
		holder.click(function(){
			if (holder.hasClass("open")) {
				$(".search-wrapper #searchform input").animate({
					width: 0
				},300, function(){
					holder.toggleClass("open");
				});
				$(".search-outer").animate({
					right: "30px"
				},300);
			} else {
				holder.toggleClass("open");
				$(".search-wrapper #searchform input").animate({
					width: "270px"
				},300, function() {
					$( ".search-wrapper #searchform input").focus();
				});
				$(".search-outer").animate({
					right: "310px"
				},300);
			}
		});
	};

	//show submenu on hover
	$.fn.menuHover = function() {
		var items = $(this).find("li.menu-item-has-children");
		$("#menu-main>li").each( function() {
			if (!$(this).hasClass('menu-item-has-children')) {
				$(this).addClass("no-child");
			}
		});
		$("#menu-main>li")
		items.addClass("ease-transition");
		items.each(function (){
			var item = $(this);
			var menuH = $(this).find("a").outerHeight();
			item.hover(function() {
				var h = item.find(".sub-menu").outerHeight() + menuH;
				item.css({'height': h});
			}, function() {
				$(this).css({'height': menuH});
			});
		});
		
	};

	$.fn.mobileMenu = function( statement) {
		$(window).load(function(){
			var h = $(window).outerHeight()-50;
			$("header .menu-tab").click(function() {

				var tab = $(this);
				var menu = $(".mobile-menu-wrapper");

				if (tab.hasClass("active")) {
					tab.removeClass("active");
					menu.stop().animate({
						height: 0
					}, {
						duration: 500,
						queue: false
					});
				} else {
					tab.addClass("active");
					menu.stop().animate({
						height: h
					}, {
						duration: 500,
						queue: false
					});
				};
			});

		});
	}

})(jQuery);
