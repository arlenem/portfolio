if ($(".module-mini-contained_accordion .accordion-outer").length) {
	$(".module-mini-contained_accordion .accordion-outer").each(function() {
		$(this).openAccordion();
	});
}

$.fn.openAccordion = function() {
	var holder = $(this);
	var title = holder.find(".accordion-title-wrapper");
	var hidden = holder.find(".accordion-hidden-text");
	var h = hidden.find(".accordion-inner").outerHeight();

	title.click(function(){
		if (title.hasClass("open")) {
			title.removeClass("open")
			hidden.stop().animate({
				height: 0
			}, 500);
		}else {
			title.addClass("open")
			hidden.stop().animate({
				height: h
			}, 500);
		};
	})
};