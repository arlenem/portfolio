<?php 
/**
 * The template for displaying Modules in the mini-module contained accordion group format
 ****************************************************************/

// Initalize the global module variable
global $module;

// Create the HTML Wrapper ?>
<section class="module-mini-contained module-mini-contained_accordion">
	<article>
		<div class="row">
			<div class="col-md-6 col-md-push-6 content-container">
				<?php if ($module["module_fields"]['contact_fields']) {
					foreach ($module["module_fields"]['contact_fields'] as $content) {
						switch ($content["acf_fc_layout"]) {
							case 'subtitle':
								echo '<h4 class="' . $content['subtitle_color'] . '">' . $content['subtitle']  . '</h4>';
								break;
							
							case 'body_copy':
								echo $content['body_copy'];
								break;

							case 'image':
								$img_srcset = wp_get_attachment_image_srcset($content['group_image']['id']);
								echo '<img src="'. $content['group_image']['sizes']['large'] .'" srcset="'.$img_srcset.'" sizes="(max-width: 900px) 100vw, 50vw" alt="'.$content['group_image']['title'] .'">';
								break;

							case 'list':
								echo '<p>' . $content['list_intro'] . '</p>';
								if ($content['list_group']) {
									foreach ($content['list_group'] as $list) {
										echo '<p>- ' .  $list['list_item'] . '</p>';
									}
								}
								break;

							case 'link':
								echo '<p><a class="a2" href="' . $content['link_url'] .'">' . $content['link_text'] . '<span></span></a></p>';
								break;

							case 'button':
								echo '<p class="button-wrapper"><a class="a3 button" href="' . $content['button_url'] .'">' . $content['button_text'] . '</a></p>';
								break;

							case 'accordion_group':
									echo '<div class="accordion-container">';
									if ($content['accordion']) {
										foreach ($content['accordion'] as $accordion) {
											echo '<div class="accordion-outer">';
												echo '<div class="accordion-title-wrapper pos-relative">';
													echo '<span class="pos-absolute">
															<svg viewBox="0 0 100 100" preserveAspectRatio="xMidYMid meet">
																<circle cx="50" cy="50" r="50" stroke="#1573b1" stroke-width="0" fill="#1573b1" />
																<line class="plus-sign" x1="50" y1="20" x2="50" y2="80" style="stroke:#E6E6E6;stroke-width:10" />
																<line x1="20" y1="50" x2="80" y2="50" style="stroke:#E6E6E6;stroke-width:10" />
															</svg>
														</span>';
													if ($accordion['accordion_title']) {
														echo '<h4>' . $accordion['accordion_title'] . '</h4>';
													}
													if ($accordion['accordion_subtitle']) {
														echo '<h6 class="dark-grey-text">' . $accordion['accordion_subtitle'] . '</h6>';
													}
												echo '</div>';
												echo '<div class="accordion-hidden-text">';
													echo '<div class="accordion-inner">';
														if ($accordion['accordion_body_copy']) {
															echo $accordion['accordion_body_copy'];
														}
													echo '</div>';
												echo '</div>';
											echo '</div>';
										}
									}
									echo '</div>';
								break;

							default:
								break;
						}
					}
				} ?>
				
			</div>
			<div class="col-md-6 col-md-pull-6">
				<?php echo '<div class="image-wrapper">';
					 if ($module["module_fields"]['image']) {
						$img_srcset = wp_get_attachment_image_srcset($module["module_fields"]['image']['id']);
						echo '<img src="'.$module["module_fields"]['image']['sizes']['large'].'" srcset="'.$img_srcset.'" sizes="(max-width: 900px) 100vw, 50vw" alt="'.$module["module_fields"]['image']['title'].'">';
					} 
				echo "</div>";
				echo '<div class="mini-wrapper';
					if ($module["module_fields"]['image']) { echo ' narrow-mini-group'; }
				echo '">';
					echo '<div class="row small-gutter">';
						if (is_array($module["module_fields"]['mini_modules_left']) && !empty($module["module_fields"]['mini_modules_left'])) {
							echo '<div class="col-sm-6">';
								foreach ($module["module_fields"]['mini_modules_left'] as $mini) {
									$term = wp_get_post_terms( $mini->ID, 'mini-module-tax');
									$module['mini']  = get_fields($mini->ID);
									echo "<div class='mini-module module-".$term[0]->slug."' id='".$mini->post_name."'>";
										get_template_part('mini-module-templates/'.$term[0]->slug.'-template');
									echo "</div>";
								}
							echo '</div>';
						} 
						if (is_array($module["module_fields"]['mini_modules_right']) && !empty($module["module_fields"]['mini_modules_right'])) {
							echo '<div class="col-sm-6">';
								foreach ($module["module_fields"]['mini_modules_right'] as $mini) {
									$term = wp_get_post_terms( $mini->ID, 'mini-module-tax');
									$module['mini']  = get_fields($mini->ID);
									echo "<div class='mini-module module-".$term[0]->slug."' id='".$mini->post_name."'>";
										get_template_part('mini-module-templates/'.$term[0]->slug.'-template');
									echo "</div>";
								}
							echo '</div>';
						} 
					echo '</div>';
				echo '</div>';
				?>
			</div>
		</div>
	</article>

	<div class="module-background pos-absolute <?php if ($module['module_fields']['background_color']) { echo $module['module_fields']['background_color']; } ?>"></div>
</section>