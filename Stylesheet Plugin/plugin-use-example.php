<?php

$class = $module['module_fields']['text_choice']. " " . $module['module_fields']['text_color'];

// Create the HTML Wrapper ?>
<article class="<?php echo implode(' ', $module['module_fields']['article_padding']); ?>">
	<div class="<?php echo $module['module_fields']['content_width']; ?>">
		<?php if (!empty($module['module_fields']['title'])) { 
			echo '<'.$module['module_fields']['title_choice'].' class="'. $module['module_fields']['title_color'] .' module-headline small-padding-bottom">'. $module['module_fields']['title'].'</'.$module['module_fields']['title_choice'].'>';
		}
		if (!empty($module['module_fields']['body'])) {
			echo '<p class="'. $class .' module-content">'. $module['module_fields']['body'].'</p>';
		}
		 ?>
	</div>
</article>