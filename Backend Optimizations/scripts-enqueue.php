<?php
function scripts_enqueue() {
	$ssURL = get_stylesheet_directory_uri();
	wp_enqueue_script( 'theme-modernizr', $ssURL . '/js/modernizr.js', array( 'jquery' ) );
    wp_enqueue_script( 'theme-main', $ssURL . '/js/main.js', array( 'jquery' ) );
    wp_enqueue_script( 'theme-prototypes', $ssURL . '/js/prototypes.js', array( 'jquery' ) );
    wp_enqueue_script( 'modules-main', $ssURL . '/main.js', array( 'jquery' ) );
    wp_enqueue_script( 'modules-prototypes', $ssURL . '/prototypes.js', array( 'jquery' ) );
    wp_enqueue_script( 'theme-cycle2', $ssURL . '/js/jquery.cycle2.js', array( 'jquery' ) );
    wp_enqueue_script( 'theme-cycle2-swipe', $ssURL . '/js/jquery.cycle2.swipe.js', array( 'jquery' ) );
    wp_enqueue_script( 'theme-cycle2-carousel', $ssURL . '/js/jquery.cycle2.carousel.js', array( 'jquery' ) );
    wp_enqueue_script( 'theme-cycle2-ios6fix', $ssURL . '/js/jquery.cycle2.ios6fix.js', array( 'jquery' ) );
    wp_enqueue_script( 'theme-slick', $ssURL . '/js/slick.min.js', array( 'jquery' ) );
    wp_enqueue_script( 'theme-masonry', $ssURL . '/js/isotope.pkgd.min.js', array( 'jquery' ) );
    wp_enqueue_script( 'theme-packery', $ssURL . '/js/packery-mode.min.js', array( 'jquery' ) );
    wp_enqueue_script( 'theme-masonry-images', $ssURL . '/js/imagesloaded.pkgd.min.js', array( 'jquery' ) );
    wp_enqueue_script( 'bootstrap-js', $ssURL . '/js/bootstrap.min.js', array( 'jquery' ) ); 


    wp_enqueue_style( 'theme-style-slick', $ssURL . "/css/slick.css" );
    wp_enqueue_style( 'theme-style-timeline', $ssURL . "/css/timeline.css" );
    wp_enqueue_style( 'theme-featherlight-css', $ssURL . "/css/featherlight.min.css" );
    wp_enqueue_style( 'bootstrap-css', $ssURL . "/css/bootstrap.min.css" );
    wp_enqueue_style( 'theme-stylesheet', $ssURL . "/style.css" );
    wp_enqueue_style( 'modules-stylesheet', $ssURL . "/themeStyles.css" );
    wp_enqueue_style( 'map-stylesheet', $ssURL . "/css/map-styles.css" );
    wp_enqueue_style( 'concierge-stylesheet', $ssURL . "/css/concierge-styles.css" );

	wp_localize_script( 'modules-prototypes', 'my_ajax_object',
   		array( 'ajax_url' => admin_url( 'admin-ajax.php' )
   	));
}

if(defined('DEVELOPMENT_MODE') && DEVELOPMENT_MODE) {
	add_action('wp_enqueue_scripts', 'scripts_enqueue');
}

function ia_adding_scripts() {
	$ssURL = get_stylesheet_directory_uri();

	wp_register_script( 'theme-js-compressed', $ssURL . '/js/compressed.js', array( 'jquery' ), 1.0, true );
	wp_enqueue_script('theme-js-compressed');

	wp_register_style( 'theme-style-compressed', $ssURL . '/css/compressed.css' );
	wp_enqueue_style('theme-style-compressed');

	wp_register_style( 'theme-stylesheet', $ssURL . "/style.css" );
	wp_enqueue_style( 'theme-stylesheet');

	wp_register_style( 'theme-style-compressed2', $ssURL . '/css/compressed2.css' );
	wp_enqueue_style('theme-style-compressed2');

	wp_localize_script( 'theme-js-compressed', 'my_ajax_object',
   		array( 'ajax_url' => admin_url( 'admin-ajax.php' )
   	));
}

if(!(defined('DEVELOPMENT_MODE') && DEVELOPMENT_MODE)) {
	// When development mode is not set, include the compressed version of files.
	add_action( 'wp_enqueue_scripts', 'ia_adding_scripts' );
}
?>