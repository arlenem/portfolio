<?php 
// Create the HTML Wrapper ?>
<article>
	<?php 
	echo '<div>';
		if($module['module_fields']['title']){
			echo '<h2 class="medium-padding-bottom">'. $module['module_fields']['title'] . '</h2>';
		}
		if ($module['module_fields']['image']) {
			$img_srcset = wp_get_attachment_image_srcset( $module['module_fields']['image']['ID']);
			$mob_srcset = wp_get_attachment_image_srcset( $module['module_fields']['mobile_image']['ID']);
			echo '<div class="image-wrapper">';
				echo "<img class='desktop-size' src='" . $module['module_fields']['image']['sizes']['fullwidthgrid'] . "' srcset='". $img_srcset ."' sizes='(max-width:767px) 0vw, 90vw'>";
				echo "<img class='mobile-size' src='" . $module['module_fields']['mobile_image']['sizes']['large'] . "' srcset='". $mob_srcset ."' sizes='(max-width:767px) 90vw, 0vw'>";
			echo "</div>";
		}			
	echo "</div>";
	?>
</article>
