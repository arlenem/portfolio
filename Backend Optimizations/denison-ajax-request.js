function callInArticle( item) {

    //create new section
    $( ".all-sections-wrapper").prepend( '<div class="ajax-loading no-size" data-id="'+ item +'"></div><div class="rest-of-sections"></div>');

    var temp = $( "div[data-id=" + item + "]");
    sidebarAjax( temp, item);
    $("html, body").animate({ scrollTop: 0});

    //Call next sections
    var flag = true;

    while( flag) {
        if( item) {
            var n = $( ".id-" + item).parent().next();
            item = n.find( "a").attr( "href");
            if( !item || $( "#" + item).length) {
                flag = false;
            }
            else{
                $( ".rest-of-sections").append( '<div class="ajax-loading no-size" data-id="'+ item +'"></div>');
                temp = $( "div[data-id=" + item + "]");
                sidebarAjax( temp, item);
            }
        }

    }

}

function sidebarAjax( temp, id) {

    // This does the ajax request
    ajaxHolder[ajaxHolder.length] = $.ajax({
        url: my_ajax_object.ajax_url,
        data: {
            'type'      : "post",
            'action'    :'issue_ajax_reqest',
            'value'     : id
        },
        success:function(data) {
            temp.removeClass( "no-size").html( data);
        }
    });

}